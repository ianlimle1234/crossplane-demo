# Crossplane: GitOps-based, Kubernetes-native Infrastructure as Code
## Setup
The examples are using Google Cloud (GCP)
Create an account in https://cloud.upbound.io/register
```
curl -sL https://raw.githubusercontent.com/crossplane/crossplane/release-1.0/install.sh | sh
```
Read the instructions from the output to finish the installation
```
k3d cluster create crossplane-demo
```
```
export GH_ORG=[...] # e.g., GitLab organization or username
export BASE_HOST=[...] # e.g., `$(kubectl get svc traefik -n kube-system -o json | jq --raw-output '.status.loadBalancer.ingress[0].ip').xip.io`

# open https://gitlab.com/ianlimle1234/crossplane-demo.git
# Fork it! 

git clone https://gitlab.com/ianlimle1234/crossplane-demo.git
cd crossplane-demo
```
### 1. Deploy Argo CD
```
cat argo-cd/base/ingress.yaml \
| sed -e "s@acme.com@argo-cd.$BASE_HOST@g" \
| tee argo-cd/overlays/production/ingress.yaml

cat production/argo-cd.yaml \
| sed -e "s@ianlimle1234@$GH_ORG@g" \
| tee production/argo-cd.yaml

cat apps.yaml \
| sed -e "s@ianlimle1234@$GH_ORG@g" \
| tee apps.yaml

git add .
git commit -m "Initial commit"
git push

kustomize build \
  argo-cd/overlays/production \
  | kubectl apply --filename -

kubectl --namespace argocd \
  rollout status \
  deployment argocd-server

export PASS=$(kubectl \
  --namespace argocd \
  get secret argocd-initial-admin-secret \
  --output jsonpath="{.data.password}" \
  | base64 --decode)

argocd login \
  --insecure \
  --username admin \
  --password $PASS \
  --grpc-web \
  argo-cd.$BASE_HOST

argocd account update-password \
  --current-password $PASS \
  --new-password admin123

kubectl apply --filename project.yaml

kubectl apply --filename apps.yaml
```
### 2. Deploy Crossplane
```
helm repo add crossplane-stable \
    https://charts.crossplane.io/stable

helm repo update

helm upgrade --install \
    crossplane crossplane-stable/crossplane \
    --namespace crossplane-system \
    --create-namespace \
    --wait
```
### 3. GCP resources (Project, Service Account, RBAC)
```
export PROJECT_ID=crossplane-demo-$(date +%Y%m%d%H%M%S)

gcloud projects create $PROJECT_ID

echo https://console.cloud.google.com/marketplace/product/google/container.googleapis.com?project=$PROJECT_ID

# Open the URL and *ENABLE* the API

export SA_NAME=ian-test
export SA="${SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"

gcloud iam service-accounts \
  create $SA_NAME \
  --project $PROJECT_ID

export ROLE=roles/admin
gcloud projects add-iam-policy-binding \
  --role $ROLE $PROJECT_ID \
  --member serviceAccount:$SA

gcloud iam service-accounts keys \
  create creds.json \
  --project $PROJECT_ID \
  --iam-account $SA

kubectl --namespace crossplane-system \
  create secret generic gcp-creds \
  --from-file key=./creds.json
```
## Create resources
```
kubectl crossplane install provider \
    crossplane/provider-gcp:v0.15.0

kubectl get providers
# Repeat the previous command until `HEALTHY` column is set to `True`

echo "apiVersion: gcp.crossplane.io/v1beta1
kind: ProviderConfig
metadata:
  name: default
spec:
  projectID: $PROJECT_ID
  credentials:
    source: Secret
    secretRef:
      namespace: crossplane-system
      name: gcp-creds
      key: key" \
| kubectl apply --filename -

cat gke.yaml

kubectl apply --filename gke.yaml

kubectl get gkeclusters

kubectl get nodepools
```
## Update resources
Here, we will update the production manifest `gke-region.yaml` to provision a multi-zone nodepool instead of a single-zone nodepool as in `gke.yaml`. Push to the remote repo and watch ArgoCD sync the live state to the desired state. Crossplane will then provision the multi-zone nodepool based on the new live state. 
```
cat gke-region.yaml
cp gke-region.yaml production/gke-region.yaml
git add .
git commit -m "Update to multi-zone nodepool"
git push origin main
```
## Destroy resources
```
rm production/gke-region.yaml
git add .
git commit -m "Delete GKE cluster"
git push origin main
gcloud projects delete $PROJECT_ID
k3d cluster delete crossplane-demo
```